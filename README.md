# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repo contains a VirtualBox .ova appliance for student use. This .ova contains the bare minimum for students to connect to a 'git' repo and has no data contained within it.

### How do I get set up? ###

* Install Oracle VirtualBox from [https://www.virtualbox.org/wiki/Downloads]
* Open a 'git' enabled command line interface (CLI)
* Make sure 'git' is installed on you local machine
* Copy the repo's clone url from [https://adam_brehm@bitbucket.org/sq213/osx.git]
* In your CLI, navigate, or Change Directory (cd), to your 'git' initiated directory
* Type, without the quotes and with the space, "git clone "
* Paste the url you just copied and press enter

### Contribution guidelines ###

* You are welcome to make suggestions to the setup instructions if you clearer and succint instructions

### Who do I talk to? ###

* Adam Brehm